package main

import (
	"regexp"

	log "github.com/sirupsen/logrus"
)

type DomainStats struct {
	Commits      int
	LinesAdded   int
	LinesDeleted int
}

type DomainsPlugin struct {
	CommitsByDomain map[string]DomainStats
	done            chan bool
}

var _ InitDependenciesPlugin = (*DomainsPlugin)(nil)
var _ OutputPlugin = (*DomainsPlugin)(nil)

func (p *DomainsPlugin) InitDependencies(plugins map[string]interface{}) {
	var commitLogPlugin *CommitLogPlugin = plugins["commitlog"].(*CommitLogPlugin)
	c := commitLogPlugin.commits.Listen(0)
	p.done = make(chan bool)
	go func() {
		re := regexp.MustCompile(".* <.*@([^>]+)>$")
		p.CommitsByDomain = make(map[string]DomainStats)
		for commit := range c {
			var commit = commit.(*Commit)
			if matches := re.FindStringSubmatch(commit.author); matches != nil {
				domainStats := p.CommitsByDomain[matches[1]]
				domainStats.Commits++
				for _, commitFile := range commit.numstat {
					domainStats.LinesAdded += commitFile.linesAdded
					domainStats.LinesDeleted += commitFile.linesDeleted
				}
				p.CommitsByDomain[matches[1]] = domainStats
			} else {
				log.Warn("Unexpected author, ignored: ", commit.author)
			}
		}
		close(p.done)
	}()
}

func (p *DomainsPlugin) Output() map[string]interface{} {
	m := make(map[string]interface{})
	for domain, domainStats := range p.CommitsByDomain {
		m[domain] = map[string]int{
			"commits":      domainStats.Commits,
			"linesAdded":   domainStats.LinesAdded,
			"linesDeleted": domainStats.LinesDeleted,
		}
	}
	return m
}
