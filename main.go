package main

import (
	"fmt"
	"os"

	"github.com/alecthomas/kingpin"
	"github.com/gosuri/uiprogress"
	log "github.com/sirupsen/logrus"
	yaml "gopkg.in/yaml.v2"
)

func createProgressBars(plugins map[string]interface{}) *uiprogress.Progress {
	progress := uiprogress.New()
	progress.SetOut(os.Stderr)
	progress.Start()

	for _, p := range plugins {
		if progressPlugin, ok := p.(ProgressPlugin); ok {
			bar := progress.AddBar(1)
			bar.Width = 50
			bar.AppendCompleted()
			bar.PrependElapsed()
			bar.PrependFunc(func(b *uiprogress.Bar) string {
				return fmt.Sprintf("%T", progressPlugin)
			})
			progressChan := make(chan int)
			progressPlugin.SetProgressChannel(progressChan)
			go func() {
				for i := range progressChan {
					bar.Set(i)
				}
			}()
			if total := progressPlugin.GetProgressTotal(); total > 0 {
				bar.Total = total
			}
		}
	}

	return progress
}

func main() {
	var (
		branch    = kingpin.Flag("branch", "Branch/rev to analyze").Short('b').Default("HEAD").String()
		debug     = kingpin.Flag("debug", "Set logging level to debug").Short('d').Bool()
		skipCache = kingpin.Flag("skip-cache", "Do not load cache").Bool()
		repo      = kingpin.Arg("repo", "Repository path, default .").Default(".").String()
	)
	kingpin.Parse()
	if *debug {
		log.SetLevel(log.DebugLevel)
	}

	pm := NewPluginManager()
	cache := Cache{
		pluginManager: pm,
	}
	var revRange string = *branch
	if *skipCache {
		log.Debug("Skipping cache as requested")
	} else if err := cache.LoadFromFile(*repo, *branch); err != nil {
		log.Warn("Failed to load cache:", err)
	} else {
		log.Infof("Cache loaded, previous commit %s", cache.Commit)
		revRange = fmt.Sprintf("%s..%s", cache.Commit, *branch)
	}
	log.Debug("Revision range is ", revRange)
	pm.Init(*repo, revRange)

	progress := createProgressBars(pm.Plugins)

	pm.Run()
	result := pm.Output()

	progress.Stop()

	cache = Cache{
		Version:       CurrentCacheVersion,
		Commit:        GitLastCommitSha1(*repo, *branch),
		pluginManager: pm,
	}
	if err := cache.SaveToFile(*repo); err != nil {
		log.Errorf("Error writing cache: %v", err)
	} else {
		log.Debugf("Wrote cache for commit %s", cache.Commit)
	}

	d, err := yaml.Marshal(&result)
	if err != nil {
		log.Fatalf("yaml.Marshal() error: %v", err)
	}
	fmt.Printf("---\n%s\n", string(d))
}
