package main

type InitDependenciesPlugin interface {
	InitDependencies(plugins map[string]interface{})
}

type InitPlugin interface {
	Init(repo, revRange string)
}

type OutputPlugin interface {
	Output() map[string]interface{}
}

type ProgressPlugin interface {
	GetProgressTotal() int
	SetProgressChannel(progressChan chan<- int)
}

type ProviderPlugin interface {
	Run()
}
