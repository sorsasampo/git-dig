package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPluginManager(t *testing.T) {
	assert := assert.New(t)

	pm := NewPluginManager()
	pm.Run()
	output := pm.Output()

	assert.Contains(output, "authors")
	assert.Contains(output, "general")
}
