git dig analyzes a git repository and outputs statistics in [YAML][].

I recommend using a query tool like [rq][] to filter the output.

## Example

    $ git dig linux |rq -yY 'at "general"'
    ---
    commits: 738019
    daysBetweenFirstAndLastCommit: 106751.99
    firstCommit: "0001-01-01T00:00:00Z"
    lastCommit: "2037-04-25T10:08:26+02:00"
    linesAdded: 47300520
    linesCurrently: 25488394
    linesDeleted: 21812126

## Features

* Really fast
* Keeps cache and can analyze only new commits

## Installation

You need to have [Go][] installed.

    go get gitlab.com/sorsasampo/git-dig

This will install `git-dig` in your `$GOPATH/bin`. See [How to Write Go Code:
The GOPATH environment variable][GOPATH] for instructions on how to add it to
your `$PATH`.

## Synopsis

```
usage: git-dig [<flags>] [<repo>]

Flags:
      --help           Show context-sensitive help (also try --help-long and --help-man).
  -b, --branch="HEAD"  Branch/rev to analyze
  -d, --debug          Set logging level to debug
      --skip-cache     Do not load cache

Args:
  [<repo>]  Repository path, default .
```

[GOPATH]: https://golang.org/doc/code.html#GOPATH
[Go]: https://golang.org/
[YAML]: https://en.wikipedia.org/wiki/YAML
[rq]: https://github.com/dflemstr/rq
