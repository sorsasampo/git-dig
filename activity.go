package main

type ActivityPlugin struct {
	DayOfWeek [7]uint
	HourOfDay [24]uint
	MonthOfYear [12]uint
	TimeZone map[string]uint
	done chan bool
}

var _ InitDependenciesPlugin = (*ActivityPlugin)(nil)
var _ OutputPlugin = (*ActivityPlugin)(nil)

func (p *ActivityPlugin) InitDependencies(plugins map[string]interface{}) {
	commitLogPlugin := plugins["commitlog"].(*CommitLogPlugin)
	p.TimeZone = make(map[string]uint)
	p.done = make(chan bool)

	c := commitLogPlugin.commits.Listen(0)
	go func() {
		for commit := range c {
			var commit = commit.(*Commit)

			// ISO 8601: monday is first day of week, so shift Weekday accordingly
			p.DayOfWeek[(commit.authordate.Weekday() + 6) % 7]++
			p.HourOfDay[commit.authordate.Hour()]++
			p.MonthOfYear[commit.authordate.Month() - 1]++
			p.TimeZone[commit.authordate.Format("-0700")]++
		}
		close(p.done)
	}()
}

func (p *ActivityPlugin) Output() map[string]interface{} {
	m := make(map[string]interface{})
	// convert into slices until this is fixed:
	// https://github.com/go-yaml/yaml/issues/259
	m["dayOfWeek"] = p.DayOfWeek[:]
	m["hourOfDay"] = p.HourOfDay[:]
	m["monthOfYear"] = p.MonthOfYear[:]
	m["timezone"] = p.TimeZone
	return m
}
