package main

type PluginManager struct {
	Plugins map[string]interface{}
}

func NewPluginManager() *PluginManager {
	pm := &PluginManager{}
	pm.Plugins = make(map[string]interface{})

	c := NewCommitLogPlugin()
	pm.Plugins["commitlog"] = c
	pm.Plugins["activity"] = &ActivityPlugin{}
	pm.Plugins["authors"] = &AuthorsPlugin{}
	pm.Plugins["domains"] = &DomainsPlugin{}
	pm.Plugins["general"] = &GeneralPlugin{}

	pm.InitDependencies()

	return pm
}

func (pm *PluginManager) Init(repo, revRange string) {
	for _, p := range pm.Plugins {
		if initPlugin, ok := p.(InitPlugin); ok {
			initPlugin.Init(repo, revRange)
		}
	}
}

func (pm *PluginManager) InitDependencies() {
	for _, p := range pm.Plugins {
		if initDependenciesPlugin, ok := p.(InitDependenciesPlugin); ok {
			initDependenciesPlugin.InitDependencies(pm.Plugins)
		}
	}
}

func (pm *PluginManager) Output() map[string]interface{} {
	result := make(map[string]interface{})
	for name, p := range pm.Plugins {
		if outputPlugin, ok := p.(OutputPlugin); ok {
			result[name] = outputPlugin.Output()
		}
	}
	return result
}

func (pm *PluginManager) Run() {
	for _, p := range pm.Plugins {
		if providerPlugin, ok := p.(ProviderPlugin); ok {
			providerPlugin.Run()
		}
	}
}
