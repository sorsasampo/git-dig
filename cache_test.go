package main

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCaching(t *testing.T) {
	assert := assert.New(t)

	const (
		CacheVersion int    = 1
		CommitSha1   string = "unittest"
		CommitAuthor string = "Unit Test <unit.test@example.com>"
	)

	var disk bytes.Buffer

	pm := NewPluginManager()
	commitLogPlugin := pm.Plugins["commitlog"].(*CommitLogPlugin)

	// Simulate Run()
	commitLogPlugin.commits.Ch <- &Commit{
		sha1:   CommitSha1,
		author: CommitAuthor,
	}
	close(commitLogPlugin.commits.Ch)

	// Call Output() for side-effect of waiting until processing
	pm.Output()
	assert.Equal(1, pm.Plugins["authors"].(*AuthorsPlugin).Authors[CommitAuthor].Commits)

	var cache = Cache{
		Version:       CacheVersion,
		Commit:        CommitSha1,
		pluginManager: pm,
	}

	assert.NoError(cache.SaveToWriter(&disk))

	pmLoaded := NewPluginManager()
	var cacheLoaded = Cache{
		pluginManager: pmLoaded,
	}
	assert.NoError(cacheLoaded.LoadFromReader(&disk))

	assert.Equal(cache.Version, cacheLoaded.Version)
	assert.Equal(cache.Commit, cacheLoaded.Commit)

	// Workaround to compare only loaded fields
	cache.pluginManager.Plugins["authors"].(*AuthorsPlugin).done = nil
	cacheLoaded.pluginManager.Plugins["authors"].(*AuthorsPlugin).done = nil
	cache.pluginManager.Plugins["general"].(*GeneralPlugin).done = nil
	cacheLoaded.pluginManager.Plugins["general"].(*GeneralPlugin).done = nil

	assert.Equal(
		cache.pluginManager.Plugins["authors"].(*AuthorsPlugin),
		cacheLoaded.pluginManager.Plugins["authors"].(*AuthorsPlugin),
	)
	assert.Equal(
		cache.pluginManager.Plugins["general"].(*GeneralPlugin),
		cacheLoaded.pluginManager.Plugins["general"].(*GeneralPlugin),
	)
}

func TestGetCacheFilename(t *testing.T) {
	assert := assert.New(t)

	filename := GetCacheFilename(".")
	assert.Regexp("^/home/.*/\\.cache/git-dig/.*/gitlab.com/sorsasampo/git-dig\\.cache$", filename)
}

func TestGitIsAncestor(t *testing.T) {
	assert := assert.New(t)

	assert.True(GitIsAncestor(".", "@^", "@"))
	assert.False(GitIsAncestor(".", "@", "@^"))
	assert.Panics(func() { GitIsAncestor(".", "invalid-rev", "@") })
}

func TestGitLastCommitSha1(t *testing.T) {
	assert := assert.New(t)

	sha1 := GitLastCommitSha1(".", "HEAD")

	assert.Equal(40, len(sha1))
}
