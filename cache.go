package main

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"os/exec"
	"os/user"
	"path"
	"path/filepath"
	"strings"
	"syscall"

	log "github.com/sirupsen/logrus"
)

const CurrentCacheVersion = 0

type Cache struct {
	Version int
	Commit  string
	Plugins map[string]json.RawMessage
	// Must be private to prevent encoding
	pluginManager *PluginManager
}

func (c *Cache) LoadFromFile(repo string, branch string) error {
	filename := GetCacheFilename(repo)

	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return err
	}

	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	if err := c.LoadFromReader(f); err != nil {
		return err
	}

	if c.Version != CurrentCacheVersion {
		return fmt.Errorf("Cache version mismatch, want = %d, got = %d", CurrentCacheVersion, c.Version)
	}

	if !GitIsAncestor(repo, c.Commit, branch) {
		return fmt.Errorf("Cache invalid, commit %s is not ancestor of %s", c.Commit, branch)
	}

	return nil
}

func (c *Cache) LoadFromReader(r io.Reader) error {
	dec := json.NewDecoder(r)
	err := dec.Decode(&c)
	if err != nil {
		return err
	}
	for name, rawPlugin := range c.Plugins {
		if err := json.Unmarshal(rawPlugin, c.pluginManager.Plugins[name]); err != nil {
			return err
		}
	}
	c.Plugins = nil
	return nil
}

func (c Cache) SaveToFile(repo string) error {
	filename := GetCacheFilename(repo)
	dir := path.Dir(filename)
	if err := os.MkdirAll(dir, 0755); err != nil {
		return err
	}
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()
	if err := c.SaveToWriter(f); err != nil {
		return err
	}
	return f.Sync()
}

func (c Cache) SaveToWriter(w io.Writer) error {
	enc := json.NewEncoder(w)

	// Marshal realPlugins into Plugins
	c.Plugins = make(map[string]json.RawMessage)
	for name, plugin := range c.pluginManager.Plugins {
		val, err := json.Marshal(plugin)
		if err != nil {
			return err
		}
		c.Plugins[name] = val
	}

	return enc.Encode(c)
}

func GetCacheFilename(repo string) string {
	absPath, err := filepath.Abs(repo)
	if err != nil {
		panic(err)
	}
	dir, filename := path.Split(absPath)

	return path.Join(getXdgCacheHome(), "git-dig", dir, filename+".cache")
}

func GitIsAncestor(repo string, ancestor string, rev string) bool {
	cmd := exec.Command("git", "merge-base", "--is-ancestor", ancestor, rev)
	cmd.Dir = repo
	if err := cmd.Run(); err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			if status, ok := exitError.Sys().(syscall.WaitStatus); ok {
				if status.ExitStatus() == 1 {
					return false
				}
			}
		}
		panic(fmt.Sprintf("git merge-base --is-ancestor failed: %v", err))
	}
	return true
}

func GitLastCommitSha1(repo string, branch string) string {
	cmd := exec.Command("git", "rev-list", "-1", branch)
	cmd.Dir = repo
	out, err := cmd.Output()
	if err != nil {
		log.Fatalf("git rev-list -1 failed: %s", err)
	}
	return strings.TrimRight(string(out), "\n")
}

func getXdgCacheHome() string {
	env := os.Getenv("XDG_CACHE_HOME")
	if len(env) == 0 {
		usr, err := user.Current()
		if err != nil {
			log.Fatal(err)
		}
		env = path.Join(usr.HomeDir, ".cache")
	}
	return env
}
