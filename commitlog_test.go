package main

import (
	"bufio"
	"flag"
	"io"
	"os"
	"strings"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

var commitlog = strings.Replace(`commit 5c8b5963cfc9f9b4faa11c84bb48b07367aecd22
tree 92c188e342ca55c4763a3a3b8f8998ec81a99eb4
author Sampo Sorsa <sorsasampo@protonmail.com> 1518566400 +0000
committer Sampo Sorsa <sorsasampo@protonmail.com> 1518566400 +0000

    Add main.go stub

13	0	main.go\x00\x00commit 6765ff0444888df4ea0bee645fb4ffb3daa8a38b
tree e3c27f2209aae632de22fbd81536a42047a1db47
parent 5c8b5963cfc9f9b4faa11c84bb48b07367aecd22
author Sampo Sorsa <sorsasampo@protonmail.com> 1518912000 +0000
committer Sampo Sorsa <sorsasampo@protonmail.com> 1518912000 +0000

    Add Commit struct

14	0	commitlog.go\x00`, "\\x00", "\x00", -1)

func TestParseCommits(t *testing.T) {
	t.Run("Empty commitlog", func(t *testing.T) {
		assert := assert.New(t)
		r := strings.NewReader("")
		c := ParseCommits(r)
		commits := make([]*Commit, 0)
		for commit := range c {
			commits = append(commits, commit)
		}
		assert.Equal(0, len(commits))
	})

	assert := assert.New(t)

	r := strings.NewReader(commitlog)
	c := ParseCommits(r)

	commits := make([]*Commit, 0)
	for commit := range c {
		commits = append(commits, commit)
	}

	const author = "Sampo Sorsa <sorsasampo@protonmail.com>"
	assert.Equal(2, len(commits), "Commit count")
	assert.Equal("5c8b5963cfc9f9b4faa11c84bb48b07367aecd22", commits[0].sha1)
	assert.Equal("92c188e342ca55c4763a3a3b8f8998ec81a99eb4", commits[0].tree)
	assert.Equal(author, commits[0].author)
	assert.Equal("2018-02-14T00:00:00Z", commits[0].authordate.Format(time.RFC3339))
	assert.Equal(author, commits[0].committer)
	assert.Equal("2018-02-14T00:00:00Z", commits[0].committerdate.Format(time.RFC3339))
	assert.Equal("Add main.go stub", commits[0].message)
	if assert.Equal(1, len(commits[0].numstat)) {
		assert.Equal(CommitFile{13, 0, "main.go"}, commits[0].numstat[0])
	}

	assert.Equal("6765ff0444888df4ea0bee645fb4ffb3daa8a38b", commits[1].sha1)
	assert.Equal([]string{"5c8b5963cfc9f9b4faa11c84bb48b07367aecd22"}, commits[1].parents)
	assert.Equal("Add Commit struct", commits[1].message)
	if assert.Equal(1, len(commits[1].numstat)) {
		assert.Equal(CommitFile{14, 0, "commitlog.go"}, commits[1].numstat[0])
	}
}

func TestParseCommitsMerge(t *testing.T) {
	assert := assert.New(t)

	commitlog := strings.Replace(`commit 234
parent 123
parent 123

    Merge test
\x00commit 345
parent 234

1	0	test_empty_message\x00`, "\\x00", "\x00", -1)

	r := strings.NewReader(commitlog)
	c := ParseCommits(r)

	commits := make([]*Commit, 0)
	for commit := range c {
		commits = append(commits, commit)
	}

	if assert.Equal(2, len(commits)) {
		assert.Equal("Merge test", commits[0].message)
		assert.Equal([]CommitFile{{1, 0, "test_empty_message"}}, commits[1].numstat)
	}
}

func Test_parseCommitHeader(t *testing.T) {
	assert := assert.New(t)

	c := Commit{}
	parseCommitHeader(&c, `commit 6765ff0444888df4ea0bee645fb4ffb3daa8a38b
tree e3c27f2209aae632de22fbd81536a42047a1db47
parent 5c8b5963cfc9f9b4faa11c84bb48b07367aecd22
author Sampo Sorsa <sorsasampo@protonmail.com> 1518912000 +0000
committer Sampo Sorsa <sorsasampo@protonmail.com> 1518912000 +0000
`)

	assert.Equal("6765ff0444888df4ea0bee645fb4ffb3daa8a38b", c.sha1)
	assert.Equal("e3c27f2209aae632de22fbd81536a42047a1db47", c.tree)
	assert.Equal([]string{"5c8b5963cfc9f9b4faa11c84bb48b07367aecd22"}, c.parents)
	assert.Equal("Sampo Sorsa <sorsasampo@protonmail.com>", c.author)
	assert.Equal("2018-02-18T00:00:00Z", c.authordate.Format(time.RFC3339))
	assert.Equal("Sampo Sorsa <sorsasampo@protonmail.com>", c.committer)
	assert.Equal("2018-02-18T00:00:00Z", c.committerdate.Format(time.RFC3339))
}

func Test_parseCommitMessage(t *testing.T) {
	assert := assert.New(t)

	msg := parseCommitMessage("    Message\n")
	assert.Equal("Message", msg)

	msg = parseCommitMessage("    Message\n    \n    Second line.\n")
	assert.Equal("Message\n\nSecond line.", msg)
}

func Test_parseCommitNumstat(t *testing.T) {
	testCases := []struct {
		description string
		numstat     string
		files       []CommitFile
	}{
		{
			"Single numstat entry",
			"123\t234\tfilename with spaces\x00",
			[]CommitFile{{123, 234, "filename with spaces"}},
		},
		{
			"Binary numstat",
			"-\t-\tbinary\x00",
			[]CommitFile{{0, 0, "binary"}},
		},
		{
			"Multiple entries",
			"1\t1\tfile1\x002\t2\tfile2\x00",
			[]CommitFile{{1, 1, "file1"}, {2, 2, "file2"}},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.description, func(t *testing.T) {
			assert := assert.New(t)

			files := parseCommitNumstat(tc.numstat)
			assert.Equal(tc.files, files)
		})
	}
}

func Test_parseGitDate(t *testing.T) {
	assert := assert.New(t)

	tm, err := parseGitDate(1518566400, "+0000")
	if assert.Nil(err) {
		assert.Equal("2018-02-14T00:00:00Z", tm.Format(time.RFC3339))
	}

	tm, err = parseGitDate(1518566400, "+0200")
	if assert.Nil(err) {
		assert.Equal("2018-02-14T02:00:00+02:00", tm.Format(time.RFC3339))
	}

	tm, err = parseGitDate(1518566400, "-0800")
	if assert.Nil(err) {
		assert.Equal("2018-02-13T16:00:00-08:00", tm.Format(time.RFC3339))
	}

	tm, err = parseGitDate(1518566400, "xxxxx")
	assert.NotNil(err)
}

func Test_readOneCommit(t *testing.T) {
	assert := assert.New(t)

	r := bufio.NewReader(strings.NewReader("commit\n\n-\tnumstat\x00\x00commit\n\n0\t0\tnumstat\x00"))
	c, err := readOneCommit(r)
	assert.Equal(nil, err)
	assert.Equal("commit\n\n-\tnumstat\x00", c)

	c, err = readOneCommit(r)
	assert.Equal(io.EOF, err)
	assert.Equal("commit\n\n0\t0\tnumstat\x00", c)
}

func Test_splitCommitLogEntry(t *testing.T) {
	testCases := []struct {
		description string
		entry       string
		header      string
		message     string
		numstat     string
	}{
		{
			"With message and numstat",
			"commit 123\n\n    Message\n\n0\t0\tfilename\x00",
			"commit 123\n",
			"    Message\n",
			"0\t0\tfilename\x00",
		},
		{
			"No message",
			"commit 123\n\n0\t0\tfilename\x00",
			"commit 123\n",
			"",
			"0\t0\tfilename\x00",
		},
		{
			"No numstat",
			"commit 123\n\n    Message\n",
			"commit 123\n",
			"    Message\n",
			"",
		},
		{
			"Neither message nor numstat",
			"commit 123\n",
			"commit 123\n",
			"",
			"",
		},
		{
			"More complex",
			"commit 123\ntree abc\n\n    Message\n    \n    Message 2nd\n\n-\t-\tbinary\x00",
			"commit 123\ntree abc\n",
			"    Message\n    \n    Message 2nd\n",
			"-\t-\tbinary\x00",
		},
	}
	for _, tc := range testCases {
		t.Run(tc.description, func(t *testing.T) {
			assert := assert.New(t)
			header, message, numstat := splitCommitLogEntry(tc.entry)

			assert.Equal(tc.header, header)
			assert.Equal(tc.message, message)
			assert.Equal(tc.numstat, numstat)
		})
	}
}

func TestMain(m *testing.M) {
	flag.Parse()
	if testing.Verbose() {
		log.SetLevel(log.DebugLevel)
	}
	os.Exit(m.Run())
}
