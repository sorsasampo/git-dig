package main

import (
	"time"
)

type Author struct {
	Commits      int
	FirstCommit  time.Time
	LastCommit   time.Time
	LinesAdded   int
	LinesDeleted int
}

type AuthorsPlugin struct {
	Authors map[string]Author
	done    chan bool
}

var _ InitDependenciesPlugin = (*AuthorsPlugin)(nil)

func (p *AuthorsPlugin) InitDependencies(plugins map[string]interface{}) {
	commitLogPlugin := plugins["commitlog"].(*CommitLogPlugin)
	p.Authors = make(map[string]Author)
	p.done = make(chan bool)

	c := commitLogPlugin.commits.Listen(0)
	go func() {
		for commit := range c {
			var commit = commit.(*Commit)
			author := p.Authors[commit.author]
			author.Commits++
			if author.FirstCommit.IsZero() || commit.authordate.Before(author.FirstCommit) {
				author.FirstCommit = commit.authordate
			}
			if commit.authordate.After(author.LastCommit) {
				author.LastCommit = commit.authordate
			}
			for _, commitFile := range commit.numstat {
				author.LinesAdded += commitFile.linesAdded
				author.LinesDeleted += commitFile.linesDeleted
			}
			p.Authors[commit.author] = author
		}
		close(p.done)
	}()
}

func (p *AuthorsPlugin) Output() map[string]interface{} {
	<-p.done
	m := make(map[string]interface{})
	for name, author := range p.Authors {
		a := make(map[string]interface{})
		a["commits"] = author.Commits
		a["firstCommit"] = author.FirstCommit
		a["lastCommit"] = author.LastCommit
		a["linesAdded"] = author.LinesAdded
		a["linesDeleted"] = author.LinesDeleted
		m[name] = a
	}
	return m
}
