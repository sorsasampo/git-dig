package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/sorsasampo/bchannel"
)

type CommitFile struct {
	linesAdded   int
	linesDeleted int
	path         string
}

type Commit struct {
	sha1          string
	tree          string
	parents       []string
	author        string
	authordate    time.Time
	committer     string
	committerdate time.Time
	message       string
	numstat       []CommitFile
}

func ParseCommits(commitlogReader io.Reader) <-chan *Commit {
	r := bufio.NewReader(commitlogReader)
	c := make(chan *Commit)
	go func() {
		for {
			commitString, err := readOneCommit(r)
			if len(commitString) == 0 {
				break
			}
			header, message, numstat := splitCommitLogEntry(commitString)
			commit := &Commit{}
			parseCommitHeader(commit, header)
			if len(message) > 0 {
				commit.message = parseCommitMessage(message)
			}
			if len(numstat) > 0 {
				commit.numstat = parseCommitNumstat(numstat)
			}
			c <- commit
			log.Debug("Commit: [%q]", commit)
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("Unexpected error: %s", err)
			}
		}
		close(c)
	}()

	return c
}

func parseCommitHeader(commit *Commit, header string) {
	re := regexp.MustCompile("^(author|committer) ([^>]*>) (\\d+) ([^ ]*)")
	for _, line := range strings.Split(strings.TrimRight(header, "\n"), "\n") {
		parts := strings.SplitN(line, " ", 2)
		switch parts[0] {
		case "commit":
			commit.sha1 = parts[1]
		case "tree":
			commit.tree = parts[1]
		case "parent":
			commit.parents = append(commit.parents, parts[1])
		case "author", "committer":
			matches := re.FindStringSubmatch(line)
			if parts[0] == "author" {
				commit.author = matches[2]
			} else if parts[0] == "committer" {
				commit.committer = matches[2]
			}
			unix, err := strconv.Atoi(matches[3])
			if err == nil {
				var tm time.Time
				tm, err = parseGitDate(unix, matches[4])
				if parts[0] == "author" {
					commit.authordate = tm
				} else if parts[0] == "committer" {
					commit.committerdate = tm
				}
			}
			// must be if, not else
			if err != nil {
				log.Printf("Failed to parse date: %s", line)
			}
		default:
			log.Debugf("Ignoring unrecognized header line: %s", line)
		}
	}
}

func parseCommitMessage(messageLines string) string {
	message := ""
	for _, line := range strings.Split(strings.TrimRight(messageLines, "\n"), "\n") {
		if strings.HasPrefix(line, "    ") {
			message += line[4:] + "\n"
		} else {
			log.Fatalf("Unexpected message line: %s", line)
		}
	}
	return strings.TrimRight(message, "\n")
}

func parseCommitNumstat(numstat string) []CommitFile {
	var files []CommitFile
	for _, line := range strings.Split(strings.TrimRight(numstat, "\x00"), "\x00") {
		matches := strings.Split(line, "\t")
		if matches == nil {
			log.Panicf("unexpected numstat: %s", line)
		}
		commitFile := CommitFile{}
		commitFile.path = matches[2]
		if linesAdded, err := strconv.Atoi(matches[0]); err == nil {
			commitFile.linesAdded = linesAdded
		}
		if linesDeleted, err := strconv.Atoi(matches[1]); err == nil {
			commitFile.linesDeleted = linesDeleted
		}
		files = append(files, commitFile)
	}
	return files
}

func parseGitDate(unix int, tz string) (time.Time, error) {
	tm := time.Unix(int64(unix), 0)
	hoursEast, err := strconv.Atoi(tz[0:3])
	if err != nil {
		return time.Time{}, fmt.Errorf("Failed to parse timezone %s", tz)
	}
	minutesEast, err := strconv.Atoi(tz[3:5])
	if err != nil {
		return time.Time{}, fmt.Errorf("Failed to parse timezone %s", tz)
	}
	secondsEastOfUTC := (time.Duration(hoursEast)*time.Hour + time.Duration(minutesEast)*time.Minute).Seconds()
	zone := time.FixedZone(tz, int(secondsEastOfUTC))
	return tm.In(zone), nil
}

// Read one commit entry from commitlog stream delimited by \x00
func readOneCommit(reader *bufio.Reader) (string, error) {
	var commitString []byte
	for {
		s, err := reader.ReadString('\x00')
		if err != nil {
			return string(append(commitString, []byte(s)...)), err
		}

		commitString = append(commitString, []byte(s)...)

		// peek next bytes, if "commit", we found the barrier
		p, err := reader.Peek(6)
		if string(p) == "commit" {
			// Remove commit separator \x00 (but only one so that
			// optional numstat filename end \x00 is kept)
			commitString = bytes.TrimSuffix(commitString, []byte("\x00"))
		}
		if string(p) == "commit" || err == io.EOF {
			return string(commitString), err
		}
	}
}

// Split commit log entry into header, message (optional), numstat (optional)
func splitCommitLogEntry(s string) (string, string, string) {
	re := regexp.MustCompile("^((?:[[:alpha:]][^\n]*\n)+)(?:\n((?: {4}[^\n]*\n)+))?(?:\n(([-0-9]+\t[-0-9]+\t[^\x00]+\x00)+))?$")
	matches := re.FindStringSubmatch(s)
	if matches != nil {
		return matches[1], matches[2], matches[3]
	} else {
		return "", "", ""
	}
}

type CommitLogPlugin struct {
	commits      *bchannel.BroadcastChannel
	progressChan chan<- int
	repo         string
	revRange     string
}

var _ InitPlugin = (*CommitLogPlugin)(nil)
var _ ProgressPlugin = (*CommitLogPlugin)(nil)

func NewCommitLogPlugin() *CommitLogPlugin {
	p := new(CommitLogPlugin)
	p.commits = bchannel.NewBroadcastChannel()
	return p
}

func (c *CommitLogPlugin) GetProgressTotal() int {
	cmd := exec.Command("git", "rev-list", "--count", c.revRange)
	cmd.Dir = c.repo
	out, err := cmd.Output()
	if err != nil {
		log.Fatalf("git rev-list --count failed: %s", err)
	}
	count, err := strconv.Atoi(strings.TrimRight(string(out), "\n"))
	if err != nil {
		log.Fatalf("git rev-list --count not a number: %s, %s", count, err)
	}
	return count
}

func (c *CommitLogPlugin) Init(repo, revRange string) {
	c.repo = repo
	c.revRange = revRange
}

func (c *CommitLogPlugin) Run() {
	cmd := exec.Command("git", "log", "--numstat", "--format=raw", "--reverse", "-z", c.revRange)
	cmd.Dir = c.repo
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatal(err)
	}
	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}

	commitsChan := ParseCommits(stdout)
	commitNumber := 0
	for commit := range commitsChan {
		c.commits.Ch <- commit
		commitNumber++
		if c.progressChan != nil {
			c.progressChan <- commitNumber
		}
	}
	close(c.commits.Ch)
	if c.progressChan != nil {
		close(c.progressChan)
	}
	cmd.Wait()
}

func (c *CommitLogPlugin) SetProgressChannel(progressChan chan<- int) {
	c.progressChan = progressChan
}
