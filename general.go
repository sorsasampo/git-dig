package main

import (
	"time"
)

type GeneralPlugin struct {
	Commits      int
	FirstCommit  time.Time
	LastCommit   time.Time
	LinesAdded   int
	LinesDeleted int
	done         chan bool
}

var _ InitDependenciesPlugin = (*GeneralPlugin)(nil)

func (p *GeneralPlugin) InitDependencies(plugins map[string]interface{}) {
	commitLogPlugin := plugins["commitlog"].(*CommitLogPlugin)
	p.done = make(chan bool)
	c := commitLogPlugin.commits.Listen(0)
	go func() {
		for commit := range c {
			var commit = commit.(*Commit)
			if p.FirstCommit.IsZero() || commit.authordate.Before(p.FirstCommit) {
				p.FirstCommit = commit.authordate
			}
			if commit.authordate.After(p.LastCommit) {
				p.LastCommit = commit.authordate
			}
			for _, commitFile := range commit.numstat {
				p.LinesAdded += commitFile.linesAdded
				p.LinesDeleted += commitFile.linesDeleted
			}
			p.Commits++
		}
		close(p.done)
	}()
}

func (p *GeneralPlugin) Output() map[string]interface{} {
	<-p.done
	m := make(map[string]interface{})
	m["commits"] = p.Commits
	m["daysBetweenFirstAndLastCommit"] = p.LastCommit.Sub(p.FirstCommit).Hours() / 24
	m["firstCommit"] = p.FirstCommit
	m["lastCommit"] = p.LastCommit
	m["linesAdded"] = p.LinesAdded
	m["linesCurrently"] = p.LinesAdded - p.LinesDeleted
	m["linesDeleted"] = p.LinesDeleted
	return m
}
